import { StatusBar } from "expo-status-bar";
import Toast from "react-native-toast-message";
import { Navigator } from "./src/navigation/navigation";
import {
  Provider as PaperProvider,
  DefaultTheme,
  ActivityIndicator,
} from "react-native-paper";
import { useEffect, useState } from "react";
import * as font from "expo-font";
import { View } from "react-native";
import { LoginProvider } from "./src/context/mainContext";

const theme: ReactNativePaper.Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: "white",
    primary: "white",
    placeholder: "white",
  },
};

export default function App() {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    font
      .loadAsync({
        "font-bold": require("./assets/fonts/PlusJakartaSans-Bold.ttf"),
        "font-medium": require("./assets/fonts/PlusJakartaSans-Medium.ttf"),
        "font-regular": require("./assets/fonts/PlusJakartaSans-Regular.ttf"),
      })
      .then(() => {
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <ActivityIndicator animating={true} color={"#000"} />
      </View>
    );
  }

  return (
    <>
      <PaperProvider theme={theme}>
        <StatusBar style="auto" />
        <LoginProvider>
          <Navigator />
        </LoginProvider>
        <Toast position="top" bottomOffset={20} />
      </PaperProvider>
    </>
  );
}
