import React from "react";

import { Cadastro } from "../pages/cadastro";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { MainScreen } from "../pages/mainScreen";
import { Registros } from "../pages/registros";
import { Favoritos } from "../pages/favoritos";

const Tab = createMaterialBottomTabNavigator();

export function MyTabs() {
  return (
    <Tab.Navigator
      barStyle={{
        backgroundColor: "#30302D",
      }}
      safeAreaInsets={{ bottom: 150, top: 150 }}
    >
      <Tab.Screen
        name="MainScreen"
        component={MainScreen}
        options={{
          title: "Home",
          tabBarIcon: "home",
        }}
      />
      <Tab.Screen
        name="Registros"
        component={Registros}
        options={{
          title: "Registros",
          tabBarIcon: "file-document",
        }}
      />
      <Tab.Screen
        name="Favoritos"
        component={Favoritos}
        options={{
          title: "Favoritos",
          tabBarIcon: "heart",
        }}
      />
    </Tab.Navigator>
  );
}
