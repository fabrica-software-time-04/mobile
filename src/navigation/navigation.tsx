import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Start } from "../pages/start";
import { Login } from "../pages/login";
import { Cadastro } from "../pages/cadastro";
import { View } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { getValueForKey, SecureEnumKeys } from "../Utils/loginUtils";
import { MyTabs } from "./myTabs";
import { useLoginContext } from "../context/mainContext";

type RootStackParamList = {
  MyTabs: undefined;
  Start: undefined;
  Login: undefined;
  Cadastro: undefined;
  MainScreen: undefined;
  Favoritos: undefined;
  Registros: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();

export const Navigator: React.FC = () => {
  const { hasLogin, setHasLogin } = useLoginContext();

  useEffect(() => {
    getValueForKey(SecureEnumKeys.TOKEN).then((value) => {
      if (value) {
        setHasLogin({ hasLogin: true });
      }
    });
  }, [hasLogin]);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={hasLogin ? "MyTabs" : "Start"}>
        <Stack.Screen
          name="MyTabs"
          options={{ headerShown: false }}
          component={MyTabs}
        />
        <Stack.Screen
          name="Cadastro"
          options={{ headerShown: false }}
          component={Cadastro}
        />
        <Stack.Screen
          name="Login"
          options={{ headerShown: false }}
          component={Login}
        />
        <Stack.Screen
          name="Start"
          options={{ headerShown: false }}
          component={Start}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
