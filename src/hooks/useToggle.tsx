import { useState } from "react";

interface IUseToggle {
  initState?: boolean;
}

export const useToggle = ({ initState = false }: IUseToggle) => {
  const [status, setStatus] = useState<boolean>(initState);

  return { status, setStatus };
};
