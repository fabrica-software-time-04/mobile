import * as SecureStore from "expo-secure-store";
import { setHeaderToken } from "../api";
import * as JWT from "jsonwebtoken";

export enum SecureEnumKeys {
  TOKEN = "TOKEN",
}

export async function saveSecureKey(key: string, value: string) {
  await SecureStore.setItemAsync(key, value);
}

export async function getValueForKey(key: string) {
  let result = await SecureStore.getItemAsync(key);

  if (result) return result;

  return null;
}

export async function removeKey(key: string) {
  await SecureStore.deleteItemAsync(key);
}

export function logout() {
  removeKey(SecureEnumKeys.TOKEN);
  setHeaderToken("");
}
