import { DefaultTheme, Theme } from "react-native-paper";

export const theme: Theme = {
    ...DefaultTheme,
    colors: {...DefaultTheme.colors, text: "white", primary: "white", placeholder: "white"}
    
}