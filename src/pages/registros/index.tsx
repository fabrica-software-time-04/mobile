import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { Searchbar } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import api from "../../api";
import { Toggle } from "../../components/Toogle";
import { getValueForKey, SecureEnumKeys } from "../../Utils/loginUtils";
import { AnimalCard } from "./animalCard";

export const Registros = (): JSX.Element => {
  const [search, setSearch] = useState<string>("");
  const [animals, setAnimals] = useState<any>([])

  useEffect(() => {
    getValueForKey
    (SecureEnumKeys.TOKEN).then((token) => {
      if (token) {
        api.get('/animal', {headers: {authorization: `bearer ${token}`}}).then((res) => {
          setAnimals(res.data);
        })
      }
    });
  }, [])

  return (
    <SafeAreaView style={{ flex: 1, paddingBottom: -35 }}>
      <ScrollView style={{ backgroundColor: "#f2f2f2" }}>
        <View style={styles.firstSection}>
          <View style={styles.header}>
            <Text style={styles.title}>Registros</Text>
            <Text style={styles.subtitle}>Animais cadastrados: {"0"}</Text>
            <Toggle
              style={{ marginTop: 25 }}
              data={[
                { text: "Animais", value: "1" },
                { text: "Filhotes", value: "2" },
              ]}
            />
            <View style={{ paddingHorizontal: 25, marginTop: 25 }}>
              <Searchbar
                value={search}
                onChangeText={(text) => setSearch(text)}
                placeholder={"Pesquisar"}
                iconColor={"#49454F"}
                style={styles.searchInputContainer}
                inputStyle={styles.searchInput}
              />
            </View>
          </View>
        </View>

        <View style={styles.cardsSection}>
          {animals.map((animal: any) => (
            <AnimalCard 
              imagem="" 
              mae={animal.motherName} 
              pai={animal.fatherName}
              nascimento={animal.birthdate} 
              nome={animal.name}
              peso={animal.weight} 
              sexo={animal.genre}
            />
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontFamily: "font-bold",
    fontSize: 18,
  },
  subtitle: {
    textAlign: "center",
    fontFamily: "font-regular",
    fontSize: 14,
    marginTop: 9,
  },
  header: {
    paddingVertical: 35,
  },
  firstSection: {
    backgroundColor: "#e9e9e9",
    borderBottomLeftRadius: 35,
    borderBottomRightRadius: 35,
    justifyContent: "center",
  },
  searchInputContainer: {
    height: 56,
    borderRadius: 30,
    paddingHorizontal: 10,
  },
  searchInput: {
    fontFamily: "font-regular",
    color: "black",
    fontSize: 16,
  },
  cardsSection: {
    paddingHorizontal: 30,
    marginTop: 50
  }
});
