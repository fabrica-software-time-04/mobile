import React from "react";
import { FlatList, ScrollView, StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Option } from "./option";

export const Mais = (): JSX.Element => {
  return (
    <SafeAreaView style={{ flex: 1, paddingBottom: -35 }}>
      <ScrollView style={{ backgroundColor: "#f2f2f2" }}>
        <View style={styles.firstSection}>
          <View style={styles.avatar}/>
          <Text style={styles.userNameText}>Username</Text>
          <Text style={styles.animaisCadText}>Animais cadastrados: {'00'}</Text>
        </View>

        <Text style={styles.titleText}>Mais serviços</Text>
        <Option title={"Meu perfil"} description={"Visualize seus dados cadastrados"} icon={"profile"}  />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  firstSection: {
    backgroundColor: "#e9e9e9",
    borderBottomLeftRadius: 35,
    borderBottomRightRadius: 35,
    justifyContent: "center",
    alignItems: 'center',
    paddingVertical: 40
  },
  avatar: {
    height: 150,
    width: 150,
    borderRadius: 75,
    backgroundColor: '#30302D'
  },
  userNameText: {
    fontSize: 25,
    fontFamily: 'font-bold',
    marginTop: 16
  },
  animaisCadText: {
    fontSize: 14,
    fontFamily: 'font-medium',
    marginTop: 6
  },
  titleText: {
    fontSize: 20,
    fontFamily: 'font-bold',
    marginTop: 15,
    textAlign: 'center'
  },
});