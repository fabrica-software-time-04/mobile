import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

interface OptionProps {
  title: string
  description: string
  icon: string
  iconColor?: string
  onClick?: () => void
}

export const Option = ({title, description, icon, iconColor, onClick = () => {}}: OptionProps): JSX.Element => {
  return (
    <View style={{paddingHorizontal: 30 }}>
    <TouchableOpacity style={styles.container}>
      <Text>Testando</Text>
    </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    justifyContent: 'center',
    backgroundColor: 'blue'
  }
})