import {
  Text,
  View,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from "react-native";
import { LinearBackground } from "../../components/LinearBackground";
import { StyleSheet } from "react-native";
import AnimalScanLogo from "../../../assets/animalLogo.png";
import { MainButton } from "../../components/Button";
import { MainTextInput } from "../../components/IconTextInput";
import { useState } from "react";
import Toast from "react-native-toast-message";
import { useNavigation } from "@react-navigation/native";
import api, { setHeaderToken } from "../../api";
import { saveSecureKey, SecureEnumKeys } from "../../Utils/loginUtils";
import { useLoginContext } from "../../context/mainContext";

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [hidePassword, setHidePassword] = useState(true);

  const { setHasLogin } = useLoginContext();

  const navigation = useNavigation<any>();

  const onChangeEmail = (value: string) => setEmail(value);
  const onChangePw = (value: string) => setPassword(value);

  const onForgotPw = () => {
    console.log("forgot pw");
  };
  const onLogin = async () => {
    try {
      const { data } = await api.post("/auth/login", {
        email: email.toLocaleLowerCase(),
        password,
      });

      const { accessToken } = data;
      await setHeaderToken(accessToken);
      await saveSecureKey(SecureEnumKeys.TOKEN, accessToken);
      setHasLogin(true);
      navigation.navigate("MyTabs");
    } catch (error: any) {
      console.log("error: ", error.response.data.message);
      Toast.show({
        type: "error",
        text1: error.response.data.message,
      });
    }
  };

  return (
    <>
      <LinearBackground>
        <KeyboardAvoidingView behavior="position" style={styles.mainContainer}>
          <Image source={AnimalScanLogo} />
          <View style={styles.buttonContainer}>
            <MainTextInput
              value={email}
              onChangeText={onChangeEmail}
              onPressIcon={() => setEmail("")}
              iconRight={"close"}
              placeholder="Email"
            />
            <View style={styles.buttonWrapper}>
              <MainTextInput
                secureTextEntry={hidePassword}
                value={password}
                onChangeText={onChangePw}
                onPressIcon={() => setHidePassword((prev) => !prev)}
                placeholder="Senha"
                iconRight={"eye"}
              />
            </View>
          </View>

          <View style={styles.afterButtonsContainer}>
            <Text style={styles.afterButtonTexts}>Esqueceu a senha?</Text>
            <TouchableOpacity
              onPress={onForgotPw}
              style={styles.clickHereContainer}
            >
              <Text style={styles.clickHereButton}>Clique aqui</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttons}>
            <View style={styles.continueButton}>
              <MainButton onPress={() => navigation.pop()}>Voltar</MainButton>
            </View>
            <View style={styles.continueButton}>
              <MainButton onPress={onLogin}>Continue</MainButton>
            </View>
          </View>
        </KeyboardAvoidingView>
      </LinearBackground>
    </>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignContent: "center",
    paddingTop: 30,
  },
  buttonContainer: {
    marginTop: 40,
    width: "80%",
    alignSelf: "center",
  },
  buttonWrapper: {
    marginTop: 15,
  },
  afterButtonsContainer: {
    alignSelf: "center",
    flexDirection: "row",
    marginTop: 15,
  },
  afterButtonTexts: {
    color: "white",
    fontSize: 16,
  },
  clickHereContainer: {
    marginLeft: 10,
  },
  buttons: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  clickHereButton: {
    color: "#FFC700",
    fontSize: 16,
  },
  continueButton: {
    marginTop: 30,
    marginHorizontal: 10,
    width: "40%",
    alignSelf: "center",
  },
});
