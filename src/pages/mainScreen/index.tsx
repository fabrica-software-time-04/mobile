import { View, Text, ScrollView, FlatList } from "react-native";
import { StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useEffect, useState } from "react";
import Action, { ActionItem } from "./action";
import ListHeader from "./listHeader";
import ItemSeparator from "./itemSeparator";
import { Searchbar } from "react-native-paper";
import { DashedButton } from "./dashedButton";

import filhoteImage from "../../../assets/images/filhote.png";
import animalImage from "../../../assets/images/animal.png";
import LogoutButton from "./logoutButton";
import { getValueForKey, SecureEnumKeys } from "../../Utils/loginUtils";
import api from "../../api";

export const MainScreen = () => {
  const [search, setSearch] = useState("");
  const [userEmail, setUserEmail] = useState("");

  const renderItem = ({ item }: { item: ActionItem }) => (
    <Action title={item.title} iconName={item.iconName} />
  );

  useEffect(() => {
    getValueForKey(SecureEnumKeys.TOKEN).then((token) => {
      if (token) {
        api
          .get("/user", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then(({ data }) => {
            setUserEmail(data.email.split("@")[0]);
          });
      }
    });
  }, []);

  return (
    <>
      <SafeAreaView style={{ flex: 1, paddingBottom: -35 }}>
        <ScrollView style={styles.scrollStyle}>
          <View style={styles.firstSection}>
            <View style={styles.identityRow}>
              <View style={styles.avatarContainer}>
                <View style={styles.avatar}></View>
              </View>

              <View style={styles.textColumns}>
                <Text style={styles.avatarTitle}>Olá {userEmail}!</Text>
                <Text style={styles.avatarDescription}>
                  Animais cadastrados: {0}
                </Text>
              </View>

              <LogoutButton />
            </View>

            <View style={styles.searchSection}>
              <Searchbar
                value={search}
                onChangeText={(text) => setSearch(text)}
                placeholder={"Pesquisar"}
                iconColor={"#49454F"}
                style={styles.searchInputContainer}
                inputStyle={styles.searchInput}
              />
            </View>

            <View style={styles.secondSection}>
              <ListHeader />
              <FlatList
                showsHorizontalScrollIndicator={false}
                ItemSeparatorComponent={ItemSeparator}
                horizontal
                data={[
                  { title: "Vacinas", iconName: "needle" },
                  { title: "Doenças", iconName: "virus-outline" },
                  { title: "Estatísticas", iconName: "chart-box-outline" },
                  { title: "Pesagem", iconName: "weight" },
                  { title: "Pesagem", iconName: "weight" },
                ]}
                contentContainerStyle={styles.contentList}
                renderItem={renderItem}
                style={{ marginTop: 20 }}
              ></FlatList>
            </View>
          </View>

          <View style={styles.buttonSection}>
            <DashedButton text="Cadastrar animal" bgImageFile={animalImage} />
            <DashedButton text="Cadastrar filhote" bgImageFile={filhoteImage} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollStyle: {
    backgroundColor: "#f2f2f2",
  },
  firstSection: {
    flex: 1,
    backgroundColor: "#e9e9e9",
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    paddingBottom: 20,
  },
  identityRow: {
    height: 120,
    flexDirection: "row",
  },
  avatarContainer: {
    alignContent: "center",
    justifyContent: "center",
    paddingHorizontal: 25,
  },
  avatar: {
    backgroundColor: "black",
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  textColumns: {
    flex: 1,
    paddingLeft: 0,
    justifyContent: "center",
  },
  avatarTitle: {
    fontSize: 18,
    marginBottom: 5,
    fontFamily: "font-bold",
  },
  avatarDescription: {
    fontFamily: "font-regular",
    fontSize: 14,
  },
  secondSection: {},
  contentList: {
    paddingHorizontal: 25,
    alignItems: "center",
  },
  searchSection: {
    paddingHorizontal: 25,
    marginBottom: 30,
  },
  searchInputContainer: {
    height: 56,
    borderRadius: 30,
    paddingHorizontal: 10,
  },
  searchInput: {
    fontFamily: "font-regular",
    color: "black",
    fontSize: 16,
  },
  buttonSection: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-evenly",
    marginTop: 30,
  },
});
