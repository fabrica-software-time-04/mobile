import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Text, StyleSheet, TouchableOpacity, View } from "react-native";
import { useLoginContext } from "../../context/mainContext";
import { logout, removeKey, SecureEnumKeys } from "../../Utils/loginUtils";

const LogoutButton = () => {
  const { setHasLogin } = useLoginContext();
  const navigation = useNavigation<any>();

  return (
    <View style={style.logoutContainer}>
      <TouchableOpacity
        hitSlop={{ bottom: 15, left: 15, right: 15, top: 15 }}
        style={style.logoutContainer}
        onPress={() => {
          logout();
          setHasLogin({ hasLogin: false });
          navigation.navigate("Start");
        }}
      >
        <Text style={style.itemContainer}>Sair</Text>
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  logoutContainer: {
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  logoutButton: {
    height: 45,
    width: 45,
    backgroundColor: "red",
  },
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    shadowColor: "black",
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 1,
      height: 2,
    },
    marginRight: 20,
  },
});

export default LogoutButton;
