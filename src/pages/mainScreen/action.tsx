import React, { ReactNode } from "react";
import { View, Text, StyleSheet } from "react-native";
import { IconButton } from "react-native-paper";

export interface ActionItem {
  title: string;
  iconName: string;
}

const Action = ({ title = "item title", iconName = '' }: ActionItem) => {
  return (
    <View style={{alignItems: 'center'}}>
      <View style={style.itemContainer}>
        <IconButton size={40} icon={iconName} color={'#30302D'}/>
      </View>
      <Text style={style.actionTitle}>{title}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",
    height: 75,
    width: 75,
    backgroundColor: "white",
    borderRadius: 30,
    shadowColor: 'black',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 1,
      height: 2
    }
  },
  actionTitle: {
    marginTop: 8,
    fontFamily: 'font-regular',
    fontSize: 12
  }
});

export default Action;
