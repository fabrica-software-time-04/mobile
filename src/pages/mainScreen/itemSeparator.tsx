import React from "react";
import { View } from "react-native";

const ItemSeparator: React.FC = () => {
  return <View style={{ height: "100%", width: 30 }} />;
};

export default ItemSeparator;
