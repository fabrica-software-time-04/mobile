import { Asset } from "expo-asset"
import { ImageBackground, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { IconButton } from "react-native-paper"

interface DashedButtonProps { 
  text: string
  onPress?: () => void
  bgImageFile?: any
}

export const DashedButton = ({text, bgImageFile, onPress = () => {}}: DashedButtonProps): JSX.Element => {
  return (
    <View style={style.container}>
      <TouchableOpacity onPress={onPress}>
        { bgImageFile && <ImageBackground source={{ uri: Asset.fromModule(bgImageFile).uri }} style={style.imageContainer} imageStyle={style.image}/> }
        <View style={{ position: 'absolute' }}>
          <View style={style.insideContainer}>
            <View style={style.iconBackground}>
              <IconButton icon={'plus'} size={40} color={'black'}/>
            </View>
            <Text style={style.text}>{text}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  )
}

const style = StyleSheet.create({
  container: {
    height: 150,
    width: 135,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 25,
  },
  imageContainer: {
    height: 150,
    width: 135,
  },
  image: {
    borderRadius: 25
  },
  insideContainer: {
    flex: 1, 
    height: 150,
    width: 135,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  iconBackground: {
    backgroundColor: 'rgba(255, 199, 0, 0.5)',
    borderRadius: 25,
    height: 50,
    width: 50,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  text: {
    fontFamily: 'font-regular',
    fontSize: 12,
    marginTop: 10
  }
})