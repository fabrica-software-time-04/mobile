import React from "react";
import { View, StyleSheet, Text } from "react-native";

const ListHeader = () => {
  return (
    <View style={styles.header}>
      <Text style={styles.titleSection}>Ações Rápidas</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 40,
    flexDirection: "column",
    justifyContent: "flex-end",
    paddingHorizontal: 15,
  },
  titleSection: {
    fontFamily: 'font-medium'
  }
});

export default ListHeader;
