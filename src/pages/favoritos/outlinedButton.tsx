import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

interface OutlinedButtonProps {
  text: string;
  onPress?: () => void;
  isSelected?: boolean;
}

export const OutlinedButton = ({
  text,
  isSelected,
  onPress = () => {},
}: OutlinedButtonProps): JSX.Element => {
  return (
    <TouchableOpacity
      style={{
        ...styles.button,
        backgroundColor: isSelected ? "#FFC700" : "#F2F2F2",
      }}
      onPress={onPress}
    >
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderWidth: 1,
    height: 30,
    borderRadius: 35,
    width: 110,
    justifyContent: "center",
    marginHorizontal: 5,
  },
  text: {
    fontSize: 12,
    fontFamily: "font-regular",
    textAlign: "center",
  },
});
