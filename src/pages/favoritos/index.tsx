import React, { useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { Searchbar } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import { Toggle } from "../../components/Toogle";
import { AnimalFavoritoCard } from "./animalFavoritoCard";
import { OutlinedButton } from "./outlinedButton";

interface Filter {
  id: string;
  filterText: string;
}

const filters: Filter[] = [
  {
    id: "1",
    filterText: "Mais recentes",
  },
  {
    id: "2",
    filterText: "Últimos 3 meses",
  },
  {
    id: "3",
    filterText: "Últimos 6 meses",
  },
];

export const Favoritos = (): JSX.Element => {
  const [search, setSearch] = useState<string>("");
  const [selectedFilter, setSelectedFilter] = useState<string>(
    filters[0].id || ""
  );

  return (
    <SafeAreaView style={{ flex: 1, paddingBottom: -35 }}>
      <ScrollView style={{ backgroundColor: "#f2f2f2" }}>
        <View style={styles.firstSection}>
          <View style={styles.header}>
            <Text style={styles.title}>Meus favoritos</Text>
            <Text style={styles.subtitle}>Animais favoritados: {"0"}</Text>
            <Toggle
              style={{ marginTop: 25 }}
              data={[
                { text: "Animais", value: "1" },
                { text: "Filhotes", value: "2" },
              ]}
            />
            <View style={{ paddingHorizontal: 25, marginTop: 25 }}>
              <Searchbar
                value={search}
                onChangeText={(text) => setSearch(text)}
                placeholder={"Pesquisar"}
                iconColor={"#49454F"}
                style={styles.searchInputContainer}
                inputStyle={styles.searchInput}
              />
            </View>
          </View>
        </View>

        <Text style={styles.filterSectionTitle}>Filtre por período</Text>
        <View style={styles.filterSection}>
          {filters.map((filter) => (
            <OutlinedButton
              text={filter.filterText}
              key={filter.id}
              isSelected={selectedFilter === filter.id}
              onPress={() => setSelectedFilter(filter.id)}
            />
          ))}
        </View>

        <View style={styles.cardsSection}>
          <AnimalFavoritoCard
            imagem=""
            mae="Nome completo do animal"
            pai="Nome completo do animal"
            nascimento={new Date()}
            nome="Nome do Animal"
            peso={500}
            raca="Raça"
            sexo="Sexo"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontFamily: "font-bold",
    fontSize: 18,
  },
  subtitle: {
    textAlign: "center",
    fontFamily: "font-regular",
    fontSize: 18,
    marginTop: 9,
  },
  header: {
    paddingVertical: 35,
  },
  firstSection: {
    backgroundColor: "#e9e9e9",
    borderBottomLeftRadius: 35,
    borderBottomRightRadius: 35,
    justifyContent: "center",
  },
  searchInputContainer: {
    height: 56,
    borderRadius: 30,
    paddingHorizontal: 10,
  },
  searchInput: {
    fontFamily: "font-regular",
    color: "black",
    fontSize: 18,
  },
  cardsSection: {
    paddingHorizontal: 30,
    marginTop: 50,
  },
  filterSection: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  filterSectionTitle: {
    fontFamily: "font-medium",
    fontSize: 18,
    textAlign: "center",
    paddingVertical: 15,
  },
});
