import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { IconButton } from "react-native-paper";

interface AnimalCardProps {
  nome: string;
  raca: string;
  nascimento: Date;
  sexo: string;
  peso: number;
  pai: string;
  mae: string;
  imagem: string;
}

export const AnimalFavoritoCard = ({
  nome,
  imagem,
  nascimento,
  peso,
  raca,
  sexo,
}: AnimalCardProps): JSX.Element => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.circlePicture} />
        <View style={{ marginLeft: 16 }}>
          <Text style={styles.nameText}>{nome}</Text>
          <Text style={styles.breedText}>{raca}</Text>
        </View>
        <View style={{ marginLeft: "auto" }}>
          <IconButton icon={"chevron-right"} color={"#30302D"} size={20} />
        </View>
      </View>

      <View style={{ paddingHorizontal: 15, paddingVertical: 5 }}>
        <View style={styles.detailSection}>
          <View>
            <Text style={styles.textDetailsTitle}>Nascimento</Text>
            <Text style={styles.textDetails}>{nascimento.toDateString()}</Text>
          </View>
          <View>
            <Text style={styles.textDetailsTitle}>Sexo</Text>
            <Text style={styles.textDetails}>{sexo}</Text>
          </View>
          <View>
            <Text style={styles.textDetailsTitle}>Peso</Text>
            <Text style={styles.textDetails}>{peso}kg</Text>
          </View>
          <View>
            <IconButton
              icon={"heart"}
              color={"black"}
              size={25}
              style={{ margin: 0 }}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    borderRadius: 12,
    height: 110,
    borderWidth: 1,
    borderColor: "#CFCFCF",
  },
  header: {
    height: 60,
    backgroundColor: "#FFC700",
    width: "100%",
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    display: "flex",
    flexDirection: "row",
    paddingHorizontal: 15,
    alignItems: "center",
  },
  circlePicture: {
    height: 40,
    width: 40,
    backgroundColor: "#30302D",
    borderRadius: 20,
  },
  nameText: {
    fontFamily: "font-medium",
    fontSize: 16,
  },
  breedText: {
    fontFamily: "font-regular",
    fontSize: 12,
  },
  detailSection: {
    justifyContent: "space-between",
    display: "flex",
    flexDirection: "row",
  },
  textDetailsTitle: {
    fontFamily: "font-medium",
    fontSize: 13,
  },
  textDetails: {
    fontFamily: "font-regular",
    fontSize: 13,
  },
});
