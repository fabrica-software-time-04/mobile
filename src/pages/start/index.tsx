import { Text, View, Image, TouchableOpacity } from "react-native";
import { LinearBackground } from "../../components/LinearBackground";
import { StyleSheet } from "react-native";
import AnimalScanLogo from "../../../assets/animalLogo.png"
import { MainButton } from "../../components/Button";
import { useNavigation } from "@react-navigation/native";


export const Start = () => {

    const navigation = useNavigation<any>()

    const onLogin = () => {
        navigation.navigate('Login', {})
    }

    const onRegister = () => {
        navigation.navigate('Cadastro', {})
    }

    const onForgotPw = () => {
        console.log("forgot pw")
    }

    return <>
        <LinearBackground>
            <View style={styles.mainContainer}>
                <Image source={AnimalScanLogo} />
                <View style={styles.buttonContainer}>
                    <MainButton onPress={onLogin}>Login</MainButton>
                    <View style={styles.buttonWrapper}>
                        <MainButton onPress={onRegister}>Cadastre-se</MainButton>
                    </View>
                </View>

                <View style={styles.afterButtonsContainer}>
                    <Text style={styles.afterButtonTexts}>Esqueceu a senha?</Text>
                    <TouchableOpacity onPress={onForgotPw} style={styles.clickHereContainer}>
                        <Text style={styles.clickHereButton}>Clique aqui</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </LinearBackground>
    </>
}

const styles = StyleSheet.create({
    mainContainer: {
        alignContent: "center",
        paddingTop: 30
    },
    buttonContainer: {
        marginTop: -40,
        width: "80%",
        alignSelf: "center"
    },
    buttonWrapper: {
        marginTop: 15
    },
    afterButtonsContainer: {
        alignSelf: "center",
        flexDirection: "row",
        marginTop: 15
    },
    afterButtonTexts: {
        color: "white",
        fontSize: 16
    },
    clickHereContainer: {
        marginLeft: 10,
    },
    clickHereButton: {
        color: "#FFC700",
        fontSize: 16
    }
})