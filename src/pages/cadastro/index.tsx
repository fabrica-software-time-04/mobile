import { View, Image, ImageBackground, ScrollView } from "react-native";
import { StyleSheet } from "react-native";
import AnimalScanLogo from "../../../assets/animalLogo.png";
import { LinearGradient } from "expo-linear-gradient";

import ImabeBg from "../../../assets/bgImage.png";
import { SafeAreaView } from "react-native-safe-area-context";
import { FlatTextInput } from "../../components/FlatTextInput";
import { useState } from "react";
import { MainButton } from "../../components/Button";
import api, { setHeaderToken } from "../../api";
import { saveSecureKey, SecureEnumKeys } from "../../Utils/loginUtils";
import { useNavigation } from "@react-navigation/native";
import { Toast } from "react-native-toast-message/lib/src/Toast";

export const Cadastro = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [birthdate, setBirthdate] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [hidePassword, setHidePasssword] = useState(true);
  const [hideConfirmPass, setHideConfirmPass] = useState(true);

  const navigation = useNavigation<any>();

  const onSubmit = async () => {
    const [dia, mes, ano] = birthdate.split("/");

    const parsedDate = {
      dia: Number(dia),
      mes: Number(mes) >= 1 ? Number(mes) - 1 : Number(mes),
      ano: Number(ano),
    };

    try {
      const data = {
        name,
        email,
        phone,
        birthdate: new Date(
          parsedDate.ano,
          parsedDate.mes,
          parsedDate.dia
        ).toISOString(),
        genre: "ecbb1be1-fe6b-4256-b9aa-02c4beda8288",
        password,
        confirmPassword,
      };

      const { data: resultData } = await api.post("/user", data);

      const { accessToken } = resultData;

      if (accessToken) {
        setHeaderToken(accessToken);
        await saveSecureKey(SecureEnumKeys.TOKEN, accessToken);
        navigation.navigate("MyTabs", {});
      }
    } catch (error: any) {
      Toast.show({
        type: "error",
        text1: error.response.data.message,
      });
    }
  };

  return (
    <>
      <ScrollView style={{ flex: 1 }}>
        <ImageBackground
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          source={ImabeBg}
        >
          <LinearGradient
            style={{ flex: 1, width: "100%" }}
            start={{ x: 0.75, y: 0.75 }}
            end={{ x: 0.0, y: 0.0 }}
            colors={["#30302DCC", "#FFC700"]}
          >
            <SafeAreaView style={{ flex: 1 }}>
              <Image
                style={{ flex: 3, alignSelf: "center" }}
                source={AnimalScanLogo}
              />

              <View style={styles.formContainer}>
                <FlatTextInput
                  placeholder="Nome e Sobrenome"
                  labelText="Nome e Sobrenome"
                  onPressIcon={() => setName("")}
                  iconRight={"close"}
                  onChangeText={(name1) => setName(name1)}
                  value={name}
                />

                <FlatTextInput
                  placeholder="Email"
                  labelText="Email"
                  onPressIcon={() => setEmail("")}
                  iconRight={"close"}
                  keyboardType="email-address"
                  onChangeText={(email) => setEmail(email)}
                  value={email}
                />

                <FlatTextInput
                  placeholder="DDD + Telefone"
                  labelText="DDD + Telefone"
                  onPressIcon={() => setPhone("")}
                  iconRight={"close"}
                  onChangeText={(phone) => setPhone(phone)}
                  value={phone}
                />

                <FlatTextInput
                  placeholder="00/00/00"
                  labelText="Nascimento"
                  onPressIcon={() => setBirthdate("")}
                  iconRight={"close"}
                  onChangeText={(birthdate) => setBirthdate(birthdate)}
                  value={birthdate}
                />

                <FlatTextInput
                  secureTextEntry={hidePassword}
                  placeholder="Senha"
                  labelText="Senha"
                  onPressIcon={() => setHidePasssword((prev) => !prev)}
                  iconRight={"eye"}
                  onChangeText={(password) => setPassword(password)}
                  value={password}
                />

                <FlatTextInput
                  secureTextEntry={hideConfirmPass}
                  placeholder="Confirmar Senha"
                  labelText="Confirmar Senha"
                  onPressIcon={() => setHideConfirmPass((prev) => !prev)}
                  iconRight={"eye"}
                  onChangeText={(confirmPass) =>
                    setConfirmPassword(confirmPass)
                  }
                  value={confirmPassword}
                />

                <View style={styles.buttons}>
                  <View style={styles.continueButton}>
                    <MainButton onPress={() => navigation.pop()}>
                      Voltar
                    </MainButton>
                  </View>
                  <View style={styles.continueButton}>
                    <MainButton onPress={onSubmit}>Continue</MainButton>
                  </View>
                </View>
              </View>
            </SafeAreaView>
          </LinearGradient>
        </ImageBackground>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    marginTop: -50,
    flex: 5,
    padding: 15,
    borderTopLeftRadius: 34,
    borderTopRightRadius: 34,
    backgroundColor: "rgba(48, 48, 45, 0.4)",
  },
  doubleField: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  buttons: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  continueButton: {
    marginTop: 30,
    marginHorizontal: 10,
    width: "40%",
    alignSelf: "center",
  },
});
