import { Platform } from 'react-native';
import { TextInput } from 'react-native-paper';
import { StyleSheet, TextInput as TxtInputRe } from 'react-native';
import React, { useRef, createRef } from 'react';

type TextInputProps = React.ComponentProps<typeof TextInput> & {
    iconRight: string;
    onPressIcon: () => void;
};

export const MainTextInput = (props: TextInputProps) => {

    const textRef = createRef<TxtInputRe>()

    return <TextInput
        mode="outlined"
        ref={textRef}
        right={<TextInput.Icon onPress={() => { props.onPressIcon(); textRef.current?.focus() }}
            style={{ marginTop: Platform.OS === "ios" ? "20%" : "60%" }}
            icon={props.iconRight} />}
        activeOutlineColor="white"
        theme={{ roundness: 100, colors: {placeholder: "black", text: "black"} }}
        selectionColor="grey"
        {...props}
        style={styles.textStyle}
    >
        {props.children}
    </TextInput>
}
const styles = StyleSheet.create({
    textStyle: {
        height: 55,
    },
})