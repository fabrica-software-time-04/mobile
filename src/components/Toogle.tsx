import React, { useEffect, useState } from "react";
import {
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";

interface DataObject {
  text: string;
  value: string;
}

interface ToggleProps {
  data: DataObject[];
  style: StyleProp<ViewStyle>;
  onChange?: (value: string) => void
}

export const Toggle = ({ data, style, onChange = () => {}}: ToggleProps): JSX.Element => {
  const [selectedValue, setSelectedValue] = useState<string>(data[0].value || '');

  useEffect(() => {
    onChange(selectedValue)
  }, [selectedValue])

  return (
    <View style={style}>
      <View style={{ alignItems: 'center'}}>
        <View style={{ ...styles.container, width: data.length * 88 + 10 }}>
          <>
            {data.map((dataObject) => (
              <TouchableOpacity
                key={dataObject.value}
                style={{
                  ...styles.button,
                  backgroundColor:
                    selectedValue === dataObject.value ? "#FFC700" : "#30302D",
                }}
                onPress={() => setSelectedValue(dataObject.value)}
              >
                <Text style={{ ...styles.text, color: selectedValue === dataObject.value ? 'black' : 'white' }}>{dataObject.text}</Text>
              </TouchableOpacity>
            ))}
          </>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-evenly",
    backgroundColor: "#30302D",
    borderWidth: 5,
    borderRadius: 35,
    borderColor: '#30302D',
    display: "flex",
    flexDirection: "row",
  },
  button: {
    borderRadius: 35,
    width: 88,
    height: 19
  },
  text: {
    textAlign: 'center',
    fontFamily: 'font-medium',
    fontSize: 12,
  }
});
