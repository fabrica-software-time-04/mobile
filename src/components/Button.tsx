import { Button, useTheme } from 'react-native-paper';
import { StyleSheet } from 'react-native';

type ButtonProps = React.ComponentProps<typeof Button>;

export const MainButton = (props: ButtonProps) => {
    const theme = useTheme()

    return <Button
        theme={{...theme, colors: { primary: "black" }}}
        contentStyle={styles.contentStyle}
        labelStyle={styles.labelStyle}
        {...props}
        style={styles.regularStyle}
    >
        {props.children}
    </Button>
}

const styles = StyleSheet.create({
    regularStyle: {
        height: 55,
        width: "100%",
        borderRadius: 100
    },
    contentStyle: {
        height: "100%",
        backgroundColor: "white"
    },
    labelStyle: {
        fontSize: 20,
        fontWeight: "normal",
        lineHeight: 30
    }
})