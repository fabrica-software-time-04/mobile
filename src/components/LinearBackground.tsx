import { LinearGradient } from "expo-linear-gradient";
import { View, ImageBackground, StyleProp, ViewStyle } from "react-native";
import ImabeBg from "../../assets/bgImage.png";
import { ReactNode } from "react";

interface LinearBackgroundProps {
    children?: ReactNode;
    styleContainer?: StyleProp<ViewStyle>
}

export const LinearBackground = ({ children, styleContainer }: LinearBackgroundProps) => {
    return <View style={{ flex: 1 }}>
        <ImageBackground style={[{ flex: 1, justifyContent: "center", alignItems: "center" }, styleContainer]} source={ImabeBg}>
            <LinearGradient
                style={{ flex: 1 }}
                start={{ x: 0.75, y: 0.75 }}
                end={{ x: 0.0, y: 0.0 }} colors={['#30302DCC', '#FFC700']}
            >
                {children}
            </LinearGradient>

        </ImageBackground>
    </View>
}