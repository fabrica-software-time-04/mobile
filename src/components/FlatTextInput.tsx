import { TextInput, useTheme } from "react-native-paper";
import { StyleSheet, TextInput as TxtInputRef } from "react-native";
import React, { useRef } from "react";

type TextInputProps = React.ComponentProps<typeof TextInput> & {
  iconRight: string;
  labelText: string;
  onPressIcon: () => void;
};

export const FlatTextInput = (props: TextInputProps) => {
  const theme = useTheme();

  const inputRef = useRef<TxtInputRef>(null);

  return (
    <TextInput
      mode="flat"
      label={props.labelText}
      ref={inputRef}
      right={
        <TextInput.Icon
          onPress={() => {
            props.onPressIcon();
          }}
          style={{ marginTop: "60%" }}
          icon={props.iconRight}
        />
      }
      underlineColor="white"
      theme={theme}
      {...props}
      style={[styles.textStyle, props.style]}
    >
      {props.children}
    </TextInput>
  );
};
const styles = StyleSheet.create({
  textStyle: {
    backgroundColor: "transparent",
    height: 55,
  },
});
