import axios from "axios";

const api = axios.create({ baseURL: "http://localhost:8080/" });

export const setHeaderToken = async (token: string) => {
  api.defaults.headers.common["Authorization"] = token;
};

export default api;
