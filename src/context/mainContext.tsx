import {
  createContext,
  ReactNode,
  SetStateAction,
  useContext,
  useMemo,
  useReducer,
  useState,
  Dispatch,
} from "react";

type LoginContextProviderProps = {
  children: React.ReactNode;
};

interface ILoginContext {
  hasLogin: boolean | undefined;
  setHasLogin: Dispatch<SetStateAction<any>>;
}

const INITIAL_STATE: ILoginContext = {
  hasLogin: false,
  setHasLogin: () => null,
};

const LoginContext = createContext(INITIAL_STATE);

export const useLoginContext = () => useContext(LoginContext);

export const LoginProvider = ({ children }: LoginContextProviderProps) => {
  const [hasLogin, setHasLogin] = useState();
  return (
    <LoginContext.Provider value={{ hasLogin, setHasLogin }}>
      {children}
    </LoginContext.Provider>
  );
};
